(function(){

  var Clicker = document.querySelector('#clicker'),
      multiplierButton = document.querySelector('#buy #multiplier'),
      highing = 1,
      upgrades = {
        multiplier: 1,
        luckyClick: 0,
        ultimate: 0,
        clickProficiency: 0
      },
      totalAmount = 0;


  function updateAmount(){
    var output = document.querySelector('#amount');

    totalAmount += highing * upgrades.multiplier;
    output.textContent = totalAmount;
  }

  var Buy = {
    multiplier: function(){
      upgrades.multiplier++;
    },
    slave: function(){},
    luckyClick: function(){},
    ultimate: function(){},
    clickProficiency: function(){}
  }

  function eventHandler(){
    multiplierButton.addEventListener('click', Buy.multiplier);

    Clicker.addEventListener('click', updateAmount);
  }

  eventHandler();

})();
